module gitee.com/chunanyong/gpress

go 1.16

require (
	github.com/blevesearch/bleve/v2 v2.3.4
	github.com/cloudwego/hertz v0.3.2
	github.com/go-ego/gse v0.70.2
)
